$fn=180;

module fan_30()
{
    color("DimGray")
    {
        difference()
        {
            union()
            {
                minkowski()
                {
                    translate([0,0,1])
                        cube([25,25,1.999], center=true);
                    cylinder(d=5,h=0.001);
                }
                translate([0,0,2])
                {
                    rotate([0,0,22.5])
                    {
                        cylinder(d=32.471,h=8.5,$fn=8);
                    }
                    for(i=[-1,1])
                    {
                        hull()
                        {
                            translate([-12.5*i,-12.5,0])
                                cylinder(d=5,h=8.5);
                            translate([12.5*i,12.5,0])
                                cylinder(d=5,h=8.5);
                        }
                    }
                }
            }
            for(i=[-1,1])
            {
                for(j=[-1,1])
                {
                    translate([12.5*i,12.5*j,-0.001])
                        cylinder(d=3,h=10.502);
                }
            }
            translate([0,0,-0.001])
            {
                cylinder(d=28,h=10.502);
            }
        }
        for(i=[0:2])
        {
            rotate([0,0,i*120])
            {
                translate([-1.25,0,-0.001])
                {
                    cube([2.5,14,2]);
                }
            }
        }
        cylinder(d=17,h=2);

        translate([0,0,3])
        {
            cylinder(d=17,h=7);
        }
        
        for(j=[0:4])
        {
            for(i=[0:127])
            {
                rotate([0,0,i*0.3 + j*72])
                {
                    translate([-0.2,0,3+(7/131)*i + sin(i/128*180)*2])
                    {
                        cube([0.4,13.8,0.2]);
                    }
                }
            }
        }
        
    }
}

fan_30();