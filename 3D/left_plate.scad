$fn=180;

difference()
{
    union()
    {
        translate([2.5,2.5,0])
        {
            minkowski()
            {
                cube([81.5,27.5,8]);
                cylinder(d=5,h=0.001);
            }
        }
        hull()
        {
//            translate([81.5,-13,0])
            translate([81.5,-58,0])
            {
                cylinder(d=10, h=8);
            }
//            translate([36,-13,0])
            translate([36,-58,0])
            {
                cylinder(d=10, h=8);
            }
            translate([81.5,5,0])
            {
                cylinder(d=10, h=8);
            }
            translate([36,5,0])
            {
                cylinder(d=10, h=8);
            }
        }
        translate([21,-10,0])
            cube([10,10,8]);
    }
    translate([21,-10,-0.001])
        cylinder(d=20,h=8.004);
    translate([14.9,19.4,-0.002])
        cube([16.7,8.7,8.004]);
    translate([64.25,19.5,-0.002])
        cube([8.5,8.5,8.004]);
    translate([14.5,4.5,-0.002])
        cube([25.5,8.5,8.004]);
    translate([55,4.5,-0.002])
        cube([15.5,8.5,8.004]);
    translate([7.5,8.75,-0.002])
        cylinder(d=3.5,h=8.004);
    translate([47.5,8.75,-0.002])
        cylinder(d=3.5,h=8.004);

    translate([75,-6,-0.002])
    {
        cylinder(d=10.5,h=8.004);
        for(i=[0:3])
        {
            rotate([0,0,45+i*90])
            {
                translate([8,0,0])
                {
                    cylinder(d=3.5,h=8.004);
                }
            }
        }
    }
    translate([47.5,-6,-0.002])
        cylinder(d=9.5,h=8.004);
    translate([82,19.5,-0.002])
        cylinder(d=3,h=8.004);
    translate([82,10.5,-0.002])
        cylinder(d=3,h=8.004);
    // Drag chain
    for(i=[-1,1])
    {
        translate([70-i*2,-50+3.5*i,-0.001])
        {
            cylinder(d=3.5,h=8.002);
            cylinder(d1=5.5,d2=0.001,h=2.75);
        }
    }
    // cable tie
    for(i=[-1,1])
    {
        translate([40,-50+6*i,-0.001])
        {
            cube([7,3,16.004],center=true);
        }
    }
}
    