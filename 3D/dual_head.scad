$fn=180;

use <cold_block.scad>
use <pipe_connector.scad>
use <fan.scad>
use <heathbreak.scad>
use <heather_block.scad>
use <nozzle.scad>
use <fan_duct.scad>
use <mountplate_v2.scad>
use <sensor.scad>
use <BLTouch_Holder.scad>

translate([0,-28,22])
{
    cold_block();
    for(i=[-1,1])
    {
        translate([9*i,-6,30.5])
            pipe_connector();
        translate([9*i,-6,-1])
            heathbreak();
        translate([9*i,-6,-4])
        {
            rotate([0,0,180])
            {
                heather_block();
            }
        }
        translate([9*i,-6,-16.5])
            nozzle();
        /*
        translate([31*i,-33,10])
            rotate([0,45*i,0])
                fan_30();
        */
    }
    translate([0,-48,15])
        rotate([90,0,0])
            fan_30();
    
    translate([0,-48,15])
        rotate([-90,0,0])
            fan_duct();
    
}

translate([0,0,54])
{
    rotate([90,0,0])
    {
        mountplate();
    }
}
/*
translate([-35,-30,0])
{
    ind_sensor_18();
}
*/
translate([-38,-24,10])
{
    rotate([0,0,90])
    {
        BLTouch();
    }
}

translate([0,-8,54])
{
    rotate([90,0,0])
    {
        BLTouch_Holder();
    }
}

// bed

color("aqua",0.3)
{
    cube([100,150,0.001],center=true);
}

