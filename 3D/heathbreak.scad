$fn=180;

module heathbreak()
{
    color("gray")
    {
        difference()
        {
            union()
            {
                cylinder(d=7,h=16);
                translate([0,0,-2])
                {
                    cylinder(d=3,h=2);
                }
                translate([0,0,-7])
                {
                    cylinder(d=6,h=5);
                }
            }
            translate([0,0,11.5])
            {
                cylinder(d=4,h=4.501);
            }
            translate([0,0,-7.001])
            {
                cylinder(d=2,h=23);
            }
        }
    }
}

heathbreak();