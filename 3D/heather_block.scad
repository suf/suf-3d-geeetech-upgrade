$fn=180;

module heather_block()
{
    color("silver")
    {
        difference()
        {
            translate([-8,-15,-11.5])
            {
                cube([16,23,11.5]);
            }
            translate([0,0,-11.501])
            {
                cylinder(d=6,h=11.502);
            }
            translate([0,5.5,-5.75])
            {
                cylinder(d=3,h=5.751);
            }
            translate([8.001,-7,-4.5])
            {
                rotate([0,-90,0])
                {
                    cylinder(d=6,h=16.002);
                }
            }
            translate([-8.001,-15.001,-5])
            {
                cube([16.002,10,1]);
            }
            translate([0,-12.5,-11.501])
            {
                cylinder(d=3,h=11.502);
            }
            translate([8.001,5.5,-5.5])
            {
                rotate([0,-90,0])
                {
                    cylinder(d=3,h=16.002);
                }
            }        
        }
    }
}

heather_block();