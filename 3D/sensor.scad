$fn=180;

module cap_sensor_12()
{
    color("orange")
    {
        cylinder(d= 10.5,h=5);
    }
    color("silver")
    {
        translate([0,0,5])
            cylinder(d=12,h=38);
        translate([0,0,5+38])
            cylinder(d=10.5,h=13);
    }
    color("black")
    {
        translate([0,0,56])
            cylinder(d=10.5,h=1);
    }
}

module cap_sensor_18()
{
    color("orange")
    {
        cylinder(d= 16.5,h=9);
    }
    color("silver")
    {
        translate([0,0,9])
            cylinder(d=18,h=45);
        translate([0,0,9+45])
            cylinder(d=16.5,h=14);
    }
    color("black")
    {
        translate([0,0,68])
            cylinder(d=16.5,h=2);
    }
}

module ind_sensor_18()
{
    color("RoyalBlue")
    {
        cylinder(d= 16.5,h=9);
        translate([0,0,63])
            cylinder(d=16.5,h=2);
    }
    color("silver")
    {
        translate([0,0,9])
            cylinder(d=18,h=40);
        translate([0,0,9+40])
            cylinder(d=16.5,h=14);
    }
}

module BLTouch()
{
    color("WhiteSmoke")
    {
        difference()
        {
            hull()
            {
                translate([0,0,35.15])
                    cube([8,11.54,2.3],center=true);
                for(i=[-1,1])
                {
                    translate([9*i,0,34])
                        cylinder(d=8,h=2.3);
                }
            }
            for(i=[-1,1])
            {
                translate([9*i,0,33.999])
                    cylinder(d=3.2,h=2.302);
            }
        }
        intersection()
        {
            union()
            {
                cylinder(d1=7,d2=13,h=5);
                translate([0,0,5])
                    cylinder(d=13,h=21.3);
            }
            cube([14,11.54,55],center=true);
        }
    }
    color("Orange")
    {
        translate([0,0,26.3])
            cylinder(d=11.53,h=7.7);
    }
    color("silver")
    {
        translate([0,0,-8])
            cylinder(d=2,h=8);
    }
}

BLTouch();