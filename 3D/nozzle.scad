$fn=180;

module nozzle()
{
    color("orange")
    {
        difference()
        {
            union()
            {
                translate([0,0,-3])
                {
                    cylinder(d=7*tan(30)*2, h=3, $fn=6);
                }
                cylinder(d=5.5, h=7.5);
                translate([0,0,-5.5])
                {
                    cylinder(d1=1,d2=4.5, h=2.5);
                }
            }
            translate([0,0,-5.502])
            {
                cylinder(d=0.5, h=6);
            }
            cylinder(d=2, h=8);
        }
    }
}

nozzle();