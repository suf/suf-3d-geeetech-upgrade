$fn=180;
use <components/microservo_SG90.scad>
use <components/opto_endstop_v1.scad>

translate([5,-7,10])
    SG90();
translate([20.75,10,45-5.25])
    rotate ([0,0,180])
        opto_endstop();
        
        
color("silver")
{
    cylinder(d=2,h=30);
}