$fn=180;

module BLTouch_Holder()
{
    difference()
    {
        union()
        {
            translate([-40,-8,0])
                cube([18,48,3]);
            translate([-43,-8,0])
                cube([12,3,30]);

            hull()
            {
                translate([-34,0,0])
                    cube([3,40,3]);
                translate([-34,-8,0])
                    cube([3,3,30]);
            }
        }
        translate([-18,22.5,0])
        {
            for(i=[-1,1])
            {
                for(j=[-1,1])
                {
                    translate([9*i,12*j,-0.001])
                    {
                        cylinder(d=4.5,h=3.002);
                    }
                }
            }
        }
        // BLTouch
        for(i=[-1,1])
        {
            translate([-38,-8.001,16+9*i])
            {
                rotate([-90,0,0])
                {
                    cylinder(d=3.5,h=3.002);
                }
            }
        }
    }
}

BLTouch_Holder();

